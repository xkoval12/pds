import argparse
import re
import Message
import socket


def convert_message(message):
    my_string_as_bytes = str.encode(message)
    type(my_string_as_bytes)
    return my_string_as_bytes


def send_message(message, ip, port):
    socket_to_write = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    byte_message = convert_message(message)
    print("sent to: " + ip + " and " + str(port))
    socket_to_write.sendto(byte_message, (ip, port))


def message_response(args, ip, port):
    message = Message.CommandMessage("command", "message")
    message.msg_data["from"] = getattr(args, 'from')
    message.msg_data["to"] = args.to
    message.msg_data["message"] = args.message
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)

def getlist_response(args, ip, port):
    message = Message.CommandMessage("command", "getlist")
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)



def peers_response(args, ip, port):
    message = Message.CommandMessage("command", "peers")
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)


def reconnect_response(args, ip, port):
    message = Message.CommandMessage("command", "reconnect")
    message.msg_data['ip'] = args.reg_ipv4
    message.msg_data['port'] = args.reg_port
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)

def database_response(args, ip, port):
    message = Message.CommandMessage("command", "database")
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)


def neighbors_response(args, ip, port):
    message = Message.CommandMessage("command", "neighbors")
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)


def connect_response(args, ip, port):
    message = Message.CommandMessage("command", "connect")
    message.msg_data['ip'] = args.reg_ipv4
    message.msg_data['port'] = args.reg_port
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)


def disconnect_response(args, ip, port):
    message = Message.CommandMessage("command", "disconnect")
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)


def sync_response(args, ip, port):
    message = Message.CommandMessage("command", "sync")
    bencoded_message = message.bencode_myself()
    send_message(bencoded_message, ip, port)


def type_to_response(argument):
    switcher = {
        "message": message_response,
        "getlist": getlist_response,
        "peers": peers_response,
        "reconnect": reconnect_response,
        "database": database_response,
        "neighbors": neighbors_response,
        "connect": connect_response,
        "disconnect": disconnect_response,
        "sync": sync_response
    }
    return switcher.get(argument, lambda: "Invalid type")


def get_file_data(file, id):
    linelist = file.readlines()
    for line in linelist:
        print(line)
        x = re.split(":", line)
        if id == x[0]:
            print(x)
            return x[1], x[2]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process rpc args')
    group = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument('--id', required=False)
    group.add_argument('--peer', action='store_true')
    group.add_argument('--node', action='store_true')
    parser.add_argument('--from', required=False)
    parser.add_argument('--to', required=False)
    parser.add_argument('--message', required=False)
    parser.add_argument('--command', required=False)
    parser.add_argument('--reg-ipv4', required=False)
    parser.add_argument('--reg-port', required=False)
    # , action='store_const', const='regPort';
    args = parser.parse_args()
    if args.peer:
        f = open("peer_data", "r")

    if args.node:
        f = open("node_data", "r")
    ip, port = get_file_data(f, args.id)
    port = int(port)
    response_method = type_to_response(args.command)
    response_method(args, ip, port)
