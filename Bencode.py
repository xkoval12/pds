import collections
import re
import string


def encode_string(string_data):
    length = len(string_data)
    data = str(length) + ":" + string_data
    return data


def encode_integer(integer_data):
    integer_data = "i" + str(integer_data) + "e"
    return integer_data


def encode_dictionary(dictionary):
    dictionary_data = "d"
    for key, value in dictionary.items():
        dictionary_data += encode_string(key)
        if isinstance(value, int):
            dictionary_data += encode_integer(value)
        elif isinstance(value, str):
            dictionary_data += encode_string(value)
        elif isinstance(value, list):
            dictionary_data += encode_list(value)
        elif isinstance(value, dict):
            dictionary_data += encode_dictionary(value)
    dictionary_data += "e"
    return dictionary_data


def encode_list(list_data):
    result = "d"
    for index, value in (list(enumerate(list_data))):
        result += encode_string(str(index))
        result += encode_dictionary(collections.OrderedDict(sorted(value.items(), key=lambda key: key[0])))
    result += "e"
    return result


def parse_integer(item_data, num_of_chars):
    key = item_data[0:num_of_chars]
    foo_string = item_data[num_of_chars + 1:]
    index = foo_string.find("e")
    value = foo_string[0:index]
    foo = foo_string[index + 1:]
    foo_of_chars = int(foo)
    return key, value, foo_of_chars


"""
 the method below is taken from:
 https://github.com/utdemir/bencoder/blob/master/bencoder.py
 modified to work with normal string and not byte strings
 all credits go to utdemir and qnnnnez
"""


def decode(s):
    def decode_first(s):
        if s.startswith(b"i"):
            match = re.match(b"i(-?\\d+)e", s)
            return int(match.group(1)), s[match.span()[1]:]
        elif s.startswith(b"l") or s.startswith(b"d"):
            l = []
            rest = s[1:]
            while not rest.startswith(b"e"):
                elem, rest = decode_first(rest)
                if not isinstance(elem, int) and not isinstance(elem, dict):
                    elem = elem.decode()
                l.append(elem)

            rest = rest[1:]
            if s.startswith(b"l"):
                return l, rest
            else:
                return {i: j for i, j in zip(l[::2], l[1::2])}, rest
        elif any(s.startswith(i.encode()) for i in string.digits):
            m = re.match(b"(\\d+):", s)
            length = int(m.group(1))
            rest_i = m.span()[1]
            start = rest_i
            end = rest_i + length
            return s[start:end], s[end:]
        else:
            raise ValueError("Malformed input.")

    if isinstance(s, str):
        s = s.encode("ascii")

    ret, rest = decode_first(s)
    if rest:
        raise ValueError("Malformed input.")
    return ret
