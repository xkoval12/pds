import argparse
import socket
import Message
import Bencode
import select
import time
import re
import signal
import sys
import os


# python3 pds19-client.py --id 5 --username abc --chat-ipv4 127.0.0.1 --chat-port 9999 --reg-ipv4 127.0.0.1 --reg-port 5005

def convert_message(message):
    my_string_as_bytes = str.encode(message)
    type(my_string_as_bytes)
    return my_string_as_bytes


class Client:
    def __init__(self, id, username, chat_ipv4, chat_port, reg_ipv4, reg_port):
        self.id = id
        self.username = username
        self.chat_ipv4 = chat_ipv4
        self.chat_port = int(chat_port)
        self.reg_ipv4 = reg_ipv4
        self.reg_port = int(reg_port)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((self.chat_ipv4, int(self.chat_port)))
        self.socket.setblocking(0)
        self.inputs = [self.socket]
        self.outputs = [self.socket]
        self.status = "start"
        self.is_connected = False
        self.ack_timer = False
        self.waiting_for_msg = False
        self.database = None
        self.to_send_message = None
        self.send_ack_bool = False
        self.mid_step = None
        self.msg_port_to_send = None
        self.msg_ip_to_send = None
        self.new_ip_to_connect = None
        self.new_port_to_connect = None
        self.queue = []
        if os.path.exists("peer_data"):
            append_write = 'a'
        else:
            append_write = 'w'
        f = open("peer_data", append_write)
        f.write(self.id + ":" + self.chat_ipv4 + ":" + str(self.chat_port) + "\n")
        f.close()
        signal.signal(signal.SIGINT, self.quit)

    def quit(self, signal, frame):
        print('You pressed Ctrl+C!')
        with open("peer_data", "r") as f:
            lines = f.readlines()
        with open("peer_data", "w") as f:
            for line in lines:
                x = re.split(":", line)
                if str(self.id) != x[0]:
                    f.write(line)
        if os.stat("peer_data").st_size == 0:
            os.remove("peer_data")
        sys.exit(0)

    @staticmethod
    def when_to_send_hello():
        now = time.time()
        send_hello = lambda: time.time() > now + 10
        return send_hello

    @staticmethod
    def when_to_receive_ack():

        return time.time()

    def HeLlOoOhEllOoo(self, socket_writeable):
        message = Message.HelloMessage("hello", self.id, self.username, self.chat_ipv4, self.chat_port)
        bencoded_message = message.bencode_myself()
        self.send_message_reg(bencoded_message, socket_writeable)

    def connect(self, writeable_socked):
        message = Message.HelloMessage("hello", self.id, self.username, self.chat_ipv4, self.chat_port)
        bencoded_message = message.bencode_myself()

        print("Connecting to Node")

        self.send_message_reg(bencoded_message, writeable_socked)
        self.is_connected = True

    def disconnect(self, writeable_socked):
        message = Message.HelloMessage("hello", self.id, self.username, "0", 0)
        self.is_connected = False
        print("Sent disconnect Hello")
        bencoded_message = message.bencode_myself()
        self.send_message_reg(bencoded_message, writeable_socked)

    def send_message_reg(self, message, writeable_socked):
        byte_message = convert_message(message)
        writeable_socked.sendto(byte_message, (self.reg_ipv4, self.reg_port))

    def send_message_chat(self, message, writeable_socked):
        byte_message = convert_message(message)
        print("sent " + message["type"] + "to: " + self.chat_ipv4 + " and " + str(self.chat_port))
        writeable_socked.sendto(byte_message, (self.chat_ipv4, self.chat_port))

    def check_if_has_req_peer(self, peer_list):
        for index, value in peer_list.items():
            if value["username"] == self.to_send_message["to"]:
                return True, value
        return False, None

    def process_message(self, data, addr):
        message_dict = Bencode.decode(data)
        if message_dict["type"] == "command":
            print("gotten command " + message_dict["command"])
            self.status = message_dict["command"]
            if message_dict["command"] == "message":
                self.to_send_message = message_dict
                self.mid_step = "getlist"
            if message_dict["command"] == "reconnect":
                self.new_ip_to_connect = message_dict['ip']
                self.new_port_to_connect = message_dict['port']
                self.mid_step = "disconnect"
        elif message_dict["type"] == "error":
            print("I have receviced an error!: " + str(message_dict['verbose']))
        elif message_dict["type"] == "list":
            if self.mid_step == "wait_getlist" and self.status == "message":
                self.waiting_for_msg = False
                isValidPeer, peer = self.check_if_has_req_peer(message_dict["peers"])
                if isValidPeer:
                    self.mid_step = "send_msg"
                    self.msg_ip_to_send = peer["ipv4"]
                    self.msg_port_to_send = peer["port"]
                else:
                    sys.stderr.write("Operation " + self.status + " was not succesfull. peer not found! reseting state...")
                    print("Operation " + self.status + " was not succesfull. reseting state...")
                    self.status = "start"

        elif message_dict["type"] == "list" and self.status == "getlist":
            self.send_ack_bool = True
            self.database = message_dict['peers']
            self.waiting_for_msg = False

        elif message_dict["type"] == "list" and self.status == "peers":
            self.send_ack_bool = True
            self.waiting_for_msg = False
            print(message_dict)
            self.status = "start"
        elif message_dict["type"] == "list" and self.status == "message":
            for peer, value in message_dict["peers"].items():
                print(value)
        elif message_dict["type"] == "message":
            print("I Have receviced a message! here it is: " + message_dict["message"])
            self.queue.append(addr)

        elif message_dict["type"] == "ack":
            if self.status == "wait_send_msg":
                self.status = "start"
                self.waiting_for_msg = False

    def start(self):
        is_hello_time = self.when_to_send_hello()
        while True:
            if time.time() > self.ack_timer + 2 and self.waiting_for_msg:
                self.waiting_for_msg = False
                sys.stderr.write("Operation " + self.status + " was not succesfull. reseting state...")
                print("Operation " + self.status + " was not succesfull. reseting state...")
                self.status = "start"
            readable, writable, exceptional = select.select(
                self.inputs, self.outputs, self.inputs)
            for readable_socket in readable:
                data, addr = readable_socket.recvfrom(1024)
                self.process_message(data, addr)
            for writeable_socked in writable:
                if is_hello_time() and self.is_connected:
                    is_hello_time = self.when_to_send_hello()
                    self.HeLlOoOhEllOoo(writeable_socked)
                elif self.status == "disconnect":
                    self.disconnect(writeable_socked)
                elif self.status == "message":
                    self.send_message(writeable_socked)
                elif self.status == "getlist":
                    self.send_get_list(writeable_socked)
                    self.status = "wait_getlist"
                    self.ack_timer = self.when_to_receive_ack()
                    self.waiting_for_msg = True
                elif self.status == "peers":
                    self.send_peers(writeable_socked)
                elif self.status == "reconnect":
                    self.send_reconnect(writeable_socked)
                elif self.send_ack_bool:
                    self.send_ack(writeable_socked)
                    self.send_ack_bool = False
                elif self.queue:
                    self.send_ack_queue(writeable_socked, self.queue.pop())
            time.sleep(0.3)

    def send_ack_queue(self, writeable_socket, addr):
        message = Message.AckMessage("ack", self.id)
        bencoded_message = message.bencode_myself()
        byte_message = str.encode(bencoded_message)
        print("sent ACK")
        writeable_socket.sendto(byte_message, (addr[0], int(addr[1])))

    def status_caller(self, status, writeable_socket):
        return {
            "disconnect": self.disconnect(writeable_socket),
            "message": self.send_message(writeable_socket),
            "getlist": self.send_get_list(writeable_socket),
            "peers": self.send_peers(writeable_socket),
            "recconect": self.send_reconnect(writeable_socket)
        }[status]

    def send_ack(self, writeabe_socket):
        message = Message.AckMessage("ack", self.id)
        bencoded_message = message.bencode_myself()
        byte_message = str.encode(bencoded_message)
        print("sent ACK")
        writeabe_socket.sendto(byte_message, (self.reg_ipv4, self.reg_port))

    def send_get_list(self, socket_writeable):
        message = Message.GetlistMessage("getlist", self.id)
        bencoded_message = message.bencode_myself()
        print("sent get list")
        self.send_message_reg(bencoded_message, socket_writeable)
        self.ack_timer = self.when_to_receive_ack()

    def send_message(self, writeable_socked):
        self.ack_timer = self.when_to_receive_ack()
        self.waiting_for_msg = True
        if self.mid_step == "getlist":
            self.send_get_list(writeable_socked)
            self.mid_step = "wait_" + self.mid_step
            return
        if self.mid_step == "send_msg":
            self.mid_step = None
            print("message has been sent")
            self.status = "wait_send_msg"
            self.send_msg(writeable_socked)

    def send_msg(self, writeable_socket):
        message = Message.MessageMessage("message", self.id, self.to_send_message["from"], self.to_send_message["to"],
                                         self.to_send_message["message"])
        bencoded_message = message.bencode_myself()
        byte_message = convert_message(bencoded_message)
        print("sent MESSAGE to: " + self.msg_ip_to_send + " and " + str(self.msg_port_to_send))
        writeable_socket.sendto(byte_message, (self.msg_ip_to_send, self.msg_port_to_send))


    def send_peers(self, writeable_socked):
        self.ack_timer = self.when_to_receive_ack()
        pass

    def send_reconnect(self, writeable_socked):

        if self.mid_step == "disconnect":
            self.disconnect(writeable_socked)
            self.reg_ipv4 = self.new_ip_to_connect
            self.reg_port = int(self.new_port_to_connect)
            self.connect(writeable_socked)
            self.mid_step = None
            self.status = "start"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process Client args')
    parser.add_argument('--id', required=False)
    parser.add_argument('--username', required=False)
    parser.add_argument('--chat-ipv4', required=False)
    parser.add_argument('--chat-port', required=False)
    parser.add_argument('--reg-ipv4', required=False)
    parser.add_argument('--reg-port', required=False)
    # , action='store_const', const='regPort';
    args = parser.parse_args()

    client = Client(args.id, args.username, args.chat_ipv4, args.chat_port, args.reg_ipv4, args.reg_port)
    client.connect(client.socket)
    client.start()
