import Bencode
import collections


def convert_data_to_msg_data(my_data, ip, port, other_data):
    dict = collections.OrderedDict()
    key = ip + "," + str(port)
    dict[key] = my_data
    result = other_data.copy()
    result.update(dict)
    return result


class Message:
    def __init__(self, msg_type):
        self.msg_data = collections.OrderedDict()
        self.msg_data['type'] = msg_type
        '''
        self.txid = txid
        self.username = username
        self.ipv4 = ipv4
        self.port = port
        self.peers = peers
        self.verbose = verbose
        self.db = db'''


    def sort_myself(self):
        sorted_msg_data = collections.OrderedDict(sorted(self.msg_data.items(), key=lambda key: key[0]))
        return sorted_msg_data

    def bencode_myself(self):
        return Bencode.encode_dictionary(self.sort_myself())


    def getlist_jsonify(self):
        json_string = "{\"type\":\"" + self.type + "\", \"txid\":" + str(self.txid)
        return json_string

    def list_jsonify(self):
        json_string = "{\"type\":\"" + self.type + "\", \"txid\":" + self.txid
        json_string += "\"peers\": {""}"
        return json_string

    def ack_jsonify(self):
        json_string = "{\"type\":\"" + self.type + "\", \"txid\":" + str(self.txid) + "}"

    def jsonify_myself(self):
        if self.type == "hello":
            return self.hello_jsonify()
        elif self.type == "getlist":
            return self.getlist_jsonify()
        elif self.type == "ack":
            return self.ack_jsonify()

class AckMessage(Message):
    def __init__(self, msg_type, txid):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid

class ErrorMessage(Message):
    def __init__(self, msg_type, txid, ErrorMessage):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid
        self.msg_data['verbose'] = ErrorMessage


class HelloMessage(Message):
    def __init__(self, msg_type, txid, username, ipv4, port):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid
        self.msg_data['username'] = username
        self.msg_data['ipv4'] = ipv4
        self.msg_data['port'] = port

class CommandMessage(Message):
    def __init__(self, msg_type, command):
        Message.__init__(self, msg_type)
        self.msg_data['command'] = command

class GetlistMessage(Message):
    def __init__(self, msg_type, txid):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid

class ListMessage(Message):
    def __init__(self, msg_type, txid, peers):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid
        self.msg_data['peers'] = peers

class MessageMessage(Message):
    def __init__(self, msg_type, txid, from_peer, to_peer, message_data):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid
        self.msg_data['from'] = from_peer
        self.msg_data['to'] = to_peer
        self.msg_data['message'] = message_data
class UpdateMessage(Message):
    def __init__(self, msg_type, txid, my_database_data, ip, port,other_database_data):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid
        self.msg_data['db'] = convert_data_to_msg_data(my_database_data, ip, port, other_database_data)

class DisconnectMessage(Message):
    def __init__(self, msg_type, txid):
        Message.__init__(self, msg_type)
        self.msg_data['txid'] = txid

class SyncMessage(Message):
    def __init__(self, msg_type):
        Message.__init__(self, msg_type)

    '''def jsonify_myself(self):
        json_string = "{\"type\":\"" + self.type + "\", \"txid\":" + str(self.txid)
        json_string += ", \"username\": \"" + self.username + "\", \"ipv4\":\""
        json_string += self.ipv4 + "\", \"port\":" + str(self.port) + "}"
        return json_string
    '''



    '''
        bencoded_string = "d" + Bencode.encode_string("ipv4")\
                          + Bencode.encode_string(self.ipv4)\
                          + Bencode.encode_string("port")\
                          + Bencode.encode_integer(self.port)\
                          + Bencode.encode_string("txid")\
                          + Bencode.encode_integer(self.txid)\
                          + Bencode.encode_string("type")\
                          + Bencode.encode_string(self.type)\
                          + Bencode.encode_string("username")\
                          + Bencode.encode_string(self.username)\
                          + "e"
        return bencoded_string
    '''
