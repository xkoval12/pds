import socket
import argparse
import Message
import threading
import Bencode
import time
import signal
import sys
import os
import select
import collections
import re

'''
Peer codes ->
    -1 = peer has been deleted via hello 0 msg
    -2 = peer does not exist in the current database
'''


# python3 pds19-node.py --id 5 --reg-ipv4 127.0.0.1 --reg-port 5005
def convert_message(message):
    my_string_as_bytes = str.encode(message)
    type(my_string_as_bytes)
    return my_string_as_bytes


class PDSnode:
    def __init__(self, node_id, ipv4, port):
        self.id = node_id
        self.ipv4 = ipv4
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self.ipv4, int(self.port)))
        self.buffer_size = 1024
        self.sock.setblocking(0)
        self.my_peer_database = []
        self.not_my_peer_database = collections.OrderedDict()
        self.neighbor_database = []
        self.outputs = [self.sock]
        self.inputs = [self.sock]
        self.msg_queue = []
        self.peer_queue = []
        self.ack_timer = False
        self.waiting_for_msg = False
        self.status = "status"
        self.neighbor_to_connect = None
        if os.path.exists("node_data"):
            append_write = 'a'
        else:
            append_write = 'w'
        f = open("node_data", append_write)
        f.write(self.id + ":" + self.ipv4 + ":" + str(self.port) + "\n")
        f.close()
        print("initialization done")
        signal.signal(signal.SIGINT, self.quit)

    def quit(self, frame, asdf):
        print('You pressed Ctrl+C!')
        with open("node_data", "r") as f:
            lines = f.readlines()
        with open("node_data", "w") as f:
            for line in lines:
                x = re.split(":", line)
                print(x)
                if str(self.id) != x[0]:
                    f.write(line)
        if os.stat("node_data").st_size == 0:
            os.remove("node_data")
        sys.exit(0)

    def create_neighbor(self, ip, port):
        now = time.time()
        print("create_neighbor_data")
        self.neighbor_database.append({
            "ipv4": ip,
            "port": port,
            "time": now
        })
        return {
            "ipv4": ip,
            "port": port,
            "time": now
        }
    def check_if_stale_neighbor(self):
        now = time.time()
        for index, neighbor in (list(enumerate(self.neighbor_database))):
            timeout = now - neighbor['time']
            if timeout > 12:
                self.remove_neighbor(neighbor)
                return

    def check_if_stale_peer(self):
        now = time.time()
        for index, peer in reversed(list(enumerate(self.my_peer_database))):
            timeout = now - peer['time']
            if timeout > 30:  # check if this works
                self.delete_peer_data(index)
                continue

    def create_update_neighbor(self, ip, port):
        now = time.time()
        for index, neighbor in (list(enumerate(self.neighbor_database))):
            timeout = now - neighbor['time']
            if timeout > 12:
                self.remove_neighbor(neighbor)
                return
            if neighbor["ipv4"] == ip and neighbor["port"] == port:
                neighbor["time"] = now
                return neighbor
        return self.create_neighbor(ip, port)

    def remove_neighbor(self, neighbor):
        self.neighbor_database.remove(neighbor)
        neighbor_db = neighbor["ipv4"] + "," + str(neighbor["port"])
        for old_database_data, old_peers in self.not_my_peer_database.items():
            if old_database_data == neighbor_db:
                del self.not_my_peer_database[neighbor_db]
    def create_peer_data(self, message_dict):
        print("create_peer_data")
        log_time = time.time()
        self.my_peer_database.append({
            "username": message_dict['username'],
            "ipv4": message_dict['ipv4'],
            "port": message_dict['port'],
            "txid": message_dict['txid'],
            "time": log_time
        })
        if self.neighbor_database:
            self.msg_queue.append(self.send_update)
            self.peer_queue.append("nothing")
        return {
            "username": message_dict['username'],
            "ipv4": message_dict['ipv4'],
            "port": message_dict['port'],
            "txid": message_dict['txid']
        }

    @staticmethod
    def when_to_receive_ack():
        return time.time()

    def update_peer_data(self, message_dict, peer):
        log_time = time.time()
        peer['username'] = message_dict['username']
        peer['ipv4'] = message_dict['ipv4']
        peer['port'] = message_dict['port']
        peer["txid"] = message_dict['txid']
        peer["time"] = log_time

        return {
            "username": message_dict['username'],
            "ipv4": message_dict['ipv4'],
            "port": message_dict['port'],
            "txid": message_dict['txid']
        }

    def send_ack(self, peer, writeable_sock):
        message = Message.AckMessage("ack", peer['txid'])
        bencoded_message = message.bencode_myself()
        byte_message = str.encode(bencoded_message)

        writeable_sock.sendto(byte_message, (peer["ipv4"], int(peer["port"])))

    def send_error(self, peer, writeable_sock):
        message = Message.ErrorMessage("error", peer['txid'], "I refuse to send list of peers, requestor is not registered to me!")
        bencoded_message = message.bencode_myself()
        byte_message = str.encode(bencoded_message)
        print("sent error: I refuse to send list of peers, requestor is not registered to me!")
        writeable_sock.sendto(byte_message, (peer["ipv4"], int(peer["port"])))

    def __eq__(self, second):
        return self.sock == second.sock

    def delete_peer_data(self, index):
        print("delete_peer_data")
        self.my_peer_database.pop(index)
        return {
            "username": "-1",
            "ipv4": "-1",
            "port": "-1",
            "txid": "-1"
        }

    def get_peer_list_from_database(self):
        peer_list = []
        for index, peer in (list(enumerate(self.my_peer_database))):
            peer_list.append({'username': peer['username'], 'ipv4': peer['ipv4'], 'port': int(peer['port'])})
        return peer_list

    def add_update_delete_user(self, message_dict):
        log_time = time.time()
        for index, peer in reversed(list(enumerate(self.my_peer_database))):
            timeout = log_time - peer['time']
            if timeout > 30:  # check if this works
                self.delete_peer_data(index)
                continue
            if message_dict['txid'] in peer['txid']:
                if message_dict['ipv4'] == "0" and message_dict['port'] == "0":
                    return self.delete_peer_data(index)
                return self.update_peer_data(message_dict, peer)
        return self.create_peer_data(message_dict)

    def hello_response(self, message_dict):
        pass

    def ack_response(self):
        self.waiting_for_msg = False

    def getlist_response(self, peer, writeable_sock):
        message = Message.ListMessage("list", int(peer['txid']), self.get_peer_list_from_database())
        bencoded_message = message.bencode_myself()
        byte_message = str.encode(bencoded_message)
        print("sent list")
        writeable_sock.sendto(byte_message, (peer["ipv4"], int(peer["port"])))

    def ack_response(self, message_dict):

        pass

    def print_database(self):
        print("This is my database of peers!")
        for peer in self.my_peer_database:
            print(peer)

    def print_neighbors(self):
        print("These are my neighbors!")
        for neighbor in self.neighbor_database:
            print(neighbor)

    def when_to_send_update(self):
        now = time.time()
        send_update = lambda: time.time() > now + 4
        return send_update

    def start(self):
        is_update_time = self.when_to_send_update()
        while True:
            if time.time() > self.ack_timer + 2 and self.waiting_for_msg:
                self.waiting_for_msg = False
                # sys.stderr.write("Operation " + self.status + " was not succesfull. reseting state...")
                print("Operation " + self.status + " was not succesfull. reseting state...")
                self.status = "start"
            readable, writable, exceptional = select.select(
                self.inputs, self.outputs, self.inputs)
            for readable_socket in readable:
                data, addr = readable_socket.recvfrom(1024)

                self.parse_message(data, addr)

            for writeable_socked in writable:
                if is_update_time() and self.neighbor_database and self.status != "connect":
                    is_update_time = self.when_to_send_update()
                    self.send_update(None, writeable_socked)
                if self.status == "database":
                    self.print_database()
                    self.status = "start"
                elif self.status == "neighbors":
                    self.print_neighbors()
                    self.status = "start"
                elif self.status == "connect":
                    self.connect_to_node(writeable_socked)
                    self.status = "start"
                elif self.status == "sync":
                    self.sync_with_nodes(writeable_socked)
                    self.status = "start"
                elif self.status == "disconnect":
                    self.disconnect_from_nodes(writeable_socked)
                    self.status = "start"
                if self.msg_queue:
                    method = self.msg_queue.pop(0)
                    method(self.peer_queue.pop(0), writeable_socked)
                else:
                    time.sleep(0.20)
                    self.check_if_stale_neighbor()
                    self.check_if_stale_peer()

            # t = threading.Thread(target=self.parse_message, args=(clientMsg))
            # t.start()
    def sync_with_nodes(self, writeable_socket):
        message = Message.SyncMessage("sync")
        bencoded_message = message.bencode_myself()
        for neighbor in self.neighbor_database:
            self.send_msg(writeable_socket, bencoded_message, neighbor["ipv4"], neighbor["port"])

    def disconnect_from_nodes(self, writeable_socket):
        message = Message.DisconnectMessage("disconnect", self.id)
        bencoded_message = message.bencode_myself()
        for neighbor in self.neighbor_database:
            self.send_msg(writeable_socket, bencoded_message, neighbor["ipv4"], neighbor["port"])
        self.neighbor_database = []
        self.not_my_peer_database = collections.OrderedDict()

    def send_update(self, peer, writeable_socket):
        message = Message.UpdateMessage("update", self.id, self.get_peer_list_from_database(), self.ipv4, self.port,
                                        self.not_my_peer_database)
        bencoded_message = message.bencode_myself()
        for neighbor in self.neighbor_database:
            self.send_msg(writeable_socket, bencoded_message, neighbor["ipv4"], neighbor["port"])
            
    def connect_to_node(self, writeable_socket):
        message = Message.UpdateMessage("update", self.id, self.get_peer_list_from_database(), self.ipv4, self.port,
                                        self.not_my_peer_database)
        bencoded_message = message.bencode_myself()
        self.send_msg(writeable_socket, bencoded_message, self.neighbor_to_connect["ipv4"],
                      self.neighbor_to_connect["port"])

    def send_msg(self, writeable_socket, message, ip, port):
        byte_message = convert_message(message)
        writeable_socket.sendto(byte_message, (ip, int(port)))

    def validate_user(self, txid):
        for peer in self.my_peer_database:
            if txid == peer['txid']:
                return peer

        return {
            "username": "-2",
            "ipv4": "-2",
            "port": "-2",
            "txid": "-2"
        }

    def add_peers_to_database(self, peers, addr):
        # me = self.ipv4 + "," + str(self.port)
        authoritative_server = addr[0] + "," + str(addr[1])

        for new_database_msg_data, new_peers in peers.items():
            if not self.not_my_peer_database:
                if new_database_msg_data == authoritative_server:
                    self.not_my_peer_database[new_database_msg_data] = new_peers
                    return
            for old_database_data, old_peers in self.not_my_peer_database.items():
                if new_database_msg_data == authoritative_server:
                    if new_database_msg_data == old_database_data:  # update data

                        for index, peer in new_peers.items():
                            for index_old, old_peer in old_peers.items():
                                if peer["username"] == old_peer["username"]:
                                    self.not_my_peer_database[authoritative_server][index_old] = peer
                                else:
                                    self.not_my_peer_database[authoritative_server][index_old + "1"] = peer
                    else:
                        self.not_my_peer_database[new_database_msg_data] = new_peers

    def parse_message(self, bencoded_message, addr):
        '''
            dostanem spravu.
            zaslem ack -> ack obsahuje identifikator.
            send_ack(message_dict["ip"])
        '''
        # CHECK IF DE-BENCODE WAS SUCCESFULL OR IT WIL BE ERROR
        message_dict = Bencode.decode(bencoded_message)


        if message_dict["type"] == "disconnect":
            for index, neighbor in (list(enumerate(self.neighbor_database))):
                if neighbor["ipv4"] == addr[0] and int(neighbor["port"]) == int(addr[1]):
                    self.remove_neighbor(neighbor)
        if message_dict["type"] == "command":
            self.status = message_dict["command"]
            if message_dict["command"] == "connect":
                self.neighbor_to_connect = self.create_update_neighbor(message_dict["ip"], message_dict["port"])
            return
        if message_dict["type"] == "update":
            self.create_update_neighbor(addr[0], str(addr[1]))
            self.add_peers_to_database(message_dict["db"], addr)
            return

        if message_dict["type"] == "hello":
            self.add_update_delete_user(message_dict)
            return

        else:
            if message_dict["type"] == "sync":
                peer = collections.OrderedDict()
                peer['ipv4'] = addr[0]
                peer['port'] = addr[1]
                peer['txid'] = "-4"
            else:
                peer = self.validate_user(message_dict['txid'])
        if peer['txid'] == "-2":
            peer['ipv4'] = addr[0]
            peer['port'] = addr[1]
            self.msg_queue.append(self.send_ack)
            self.peer_queue.append(peer)
            self.msg_queue.append(self.send_error)
            self.peer_queue.append(peer)
            return

        self.msg_queue.append(self.send_ack)
        self.peer_queue.append(peer)

        self.peer_queue.append(peer)
        self.msg_queue.append(self.type_to_response(message_dict['type']))
        self.ack_timer = self.when_to_receive_ack()
        self.waiting_for_msg = True

    def type_to_response(self, argument):
        switcher = {
            "hello": self.hello_response,
            "getlist": self.getlist_response,
            "ack": self.ack_response,
            "sync": self.sync_response
        }
        response_method = switcher.get(argument, lambda: "Invalid type")
        return response_method

    def sync_response(self, peer, writeable_sock):
        message = Message.UpdateMessage("update", self.id, self.get_peer_list_from_database(), self.ipv4, self.port,
                                        self.not_my_peer_database)
        bencoded_message = message.bencode_myself()
        self.send_msg(writeable_sock, bencoded_message, peer["ipv4"], peer["port"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process Client args')
    parser.add_argument('--id', required=False)
    parser.add_argument('--reg-ipv4', required=False)
    parser.add_argument('--reg-port', required=False)
    args = parser.parse_args()
    PDSnode(args.id, args.reg_ipv4, args.reg_port).start()
